<?php

namespace Tests;

use App\Models\MediaLibrary;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function testInsertComment()
    {
        $commentData = [
            "api_token" => "LBIGAQZOysmmMJVczow16LgOztxvuhygpUN1U9RSebEfppI7DxKaJq2A8BZ6",
            "author_id" => "1",
            "content" => "Some Test Content",
            "title" => "Test Title",
        ];

        $this->json('POST', 'api/v1/insertcomment/', $commentData, ['Accept' => 'application/json'])
            ->assertStatus(201)
            ->assertJsonStructure([
                "user" => [
                    'author_id',
                    'content',
                    'title',
                    'created_at',
                    'updated_at',
                ],
                "access_token",
                "message"
            ]);
    }
}

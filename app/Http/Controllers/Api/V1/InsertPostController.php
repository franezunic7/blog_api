<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

/**
 * @OA\Post(
 * path="/insertpost",
 * summary="Insert Post",
 * description="Inserts new blog post",
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass post data",
 *    @OA\JsonContent(
 *       required={"author_id","title","content"},
 *       @OA\Property(property="author_id", type="integer", example="1"),
 *       @OA\Property(property="title", type="string", example="Title"),
 *       @OA\Property(property="content", type="string", example="Content"),
 *    ),
 * ),
 * @OA\Response(
 *    response=200,
 *    description="Success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Success")
 *        )
 *     )
 * )
 */
class InsertPostController extends Controller
{
    public function insertpost(Request $request)
    {
        $post = new Post();
        $post->author_id = $request->input('author_id');
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->posted_at = date('Y-m-d H:i:s');
        $post->save();

        return response()->json($post);
    }

}

<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;

/**
 * @OA\Post(
 * path="/insertcomment",
 * summary="Insert Comment",
 * description="Inserts new blog post comment",
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass comment data",
 *    @OA\JsonContent(
 *       required={"author_id","post_id"},
 *       @OA\Property(property="author_id", type="integer", example="1"),
 *       @OA\Property(property="post_id", type="integer", example="1"),
 *       @OA\Property(property="content", type="string", example="Content"),
 *    ),
 * ),
 * @OA\Response(
 *    response=200,
 *    description="Success",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Success")
 *        )
 *     )
 * )
 */
class InsertCommentController extends Controller
{
    public function insertcomment(Request $request)
    {
        $comment = new Comment();
        $comment->author_id = $request->input('author_id');
        $comment->post_id = $request->input('post_id');
        $comment->content = $request->input('content');
        $comment->save();

        return response()->json($comment);
    }

}
